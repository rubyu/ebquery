# NAME

```
qbquery -- Command Line Interface for Epwing(Electronic Publishing-WING)
```

# SYNOPSIS

```
java -jar ebquery.jar [-h | --help]
java -jar ebquery.jar [-f format] [-m modules] [--ebmap file] -d dir word [word ...]
```

# DESCRIPTION
The **ebquery** command executes a query for an Epwing dictionary, and prints results to standard output.

```
-d dir       : Path to the directory containing Epwing's CATALOGS file.
words        : Query words.
-f format    : Output format. The default value is 'text'. Allowed values are 'text' and 'html'.
-m modules   : Modules used for rendering the output. The default value is 'tx,ec,ls'. Modules
               are en, sb, sp, in, nb, em, dc, kw, ls, ec, mg, cg, sd and tx.
--ebmap file : Path to the EBWin's GAIJI mapping file.
-h (--help)  : Print help.
```

If no word is specified, then the standard input is read.

## Usage

- The standard form of the **ebquery** command, is

```
java -jar ebquery.jar -d dir word
```

This takes two arguments, a directory of a Epwing dictionary and a word.

## Format

The allowed values for **-f** *format* are `text` and `html`. The default value for **-f** is `text`.

So the command,

```
java -jar ebquery.jar -d dir word
```

is equivalent to

```
java -jar ebquery.jar -f text -d dir word
```

## Modules

The **ebquery** command is designed for flexible formatting of the output.
Each component of the Epwing renderer is devided into a module.
So you can choose modules you want to use.

The **-m** *modules* option takes a comma separated string of modules. The default value for **-m** is `tx,ec,ls`.
The modules `en`, `sb`, `sp`, `in`, `nb`, `em`, `dc`, `kw`, `ls`, `ec`, `mg`, `cg`, `sd` and `tx` are now available.

If you want to print gorgeously, the command should look like following:

```
java -jar ebquery.jar -f html -m en,sb,sp,in,nb,em,dc,kw,ls,ec,mg,cg,sd,tx -d dir word
```

For more details on modules, see [*Format of the output*](#markdown-header-format-of-the-output).

## External Character
Using **--ebmap** *file* option you can convert an external character to unicode characters.
ebqery is compatible with the map files that
[EBWin](http://www31.ocn.ne.jp/~h_ishida/EBPocket.html)/[EBWin4](http://ebstudio.info/manual/EBWin4/EBWin4.html)
installs to "%APPDATA%\Roaming\EBWin\GAIJI" or "%APPDATA%\Roaming\EBWin4\GAIJI",
but there is some differences in ebquery's intepretation on the external character replacement from EBWin's.
At first, **ebquery only uses the first and the second column** as the identifier and the replacement;
the third column, for compatibility with non-unicode system, will be ignored.
Secondly, **ebquery supports the replacement longer than 3 characters**, on the other hand
EBWin/EBWin4 only supports 3 characters or less.


### The details of the map file
[外字定義ファイル GAIJI/*.map](http://ebstudio.info/manual/EBPocket/0_0_4_4.html)

## Format of the output


| EPWING               | MODULE | HTML           | TEXT
| -------------------- | ------ | -------------- | ---------------- |
| Entry                | en     | span.ebquery   | -                |
| Text                 | tx     | text           | text             |
| Subscript            | sb     | span.ebsb      | -                |
| Superscript          | sp     | span.ebsp      | -                |
| Indent               | in     | span.ebin      | -                |
| NoNewLine            | nb     | span.ebnb      | -                |
| Emphasis             | em     | span.ebem      | -                |
| Decoration/Italic    | dc     | span.ebit      | -                |
| Decoration/Bold      | dc     | span.ebbo      | -                |
| Decoration/Underline | dc     | span.ebul      | -                |
| Candidate            | -      | -              | -                |
| Reference            | -      | -              | -                |
| Keyword              | kw     | span.ebkw      | -                |
| NewLine              | ls     | br             | newline          |
| Gaiji                | ec     | img.ebec       | text or 0xFFFD   |
| MonoGraphic          | mg     | img.ebmg       | -                |
| InlineColorGraphic   | cg     | img.ebcg       | -                |
| ColorGraphic         | cg     | img.ebcg       | -                |
| Sound                | sd     | audio.ebsd     | -                |
| Movie                | -      | -              | -                |
| GraphicReference     | -      | -              | -                |
| ImagePage            | -      | -              | -                |
| ClickableArea        | -      | -              | -                |
| EBXACGaiji           | -      | -              | -                |


The output does not include any style elements. Then, for exact display,
you need to specify the CSS manually as follows:

```css
.ebquery { display: block; }
.ebsb { vertical-align: sub; }
.ebsp { vertical-align: super; }
.ebin { display: block; margin-left: 1em; }
.ebnb { white-space: nowrap; }
.ebem { font-weight: bold; }
.ebit { font-style: italic; }
.ebbo { font-weight: bold; }
.ebul { text-decoration: underline; }
.ebkw { font-weight: bold; }
.ebec { vertical-align: bottom; }
```

Note: The detail method of "Decoration" isn't determined on the specification sheet for JIS X4081.
So there can be differences among the display of Epwing viewers,
the page of the paper version of the dictionary and the output of ebquery applied this CSS.
If you need, modify it as you like.

## Encoding
When using on Windows or other non-unicode platform,
you need to change the "file.encoding" property to "utf-8" as follows:

```
> java -Dfile.encoding=utf-8 -jar ebquery.jar ...
```

Otherwise, all characters that the default file encoding doesn't support will be converted to "?".

```
//output may contain '?'
> java -jar ebquery.jar ... word(local-encoding) > output(local-encoding).txt
```

```
//output may contain '?'
> type input(local-encoding).txt | java -jar ebquery.jar ... > output(local-encoding).txt
```

After that, the following instructions work fine.

```
> java -Dfile.encoding=utf-8 -jar ebquery.jar ... word(local-encoding) > output(utf-8).txt
> type input(utf-8).txt | java -Dfile.encoding=utf-8 -jar ebquery.jar ... > output(utf-8).txt
```

However, in the following case, non-ascii words in the input will be garbled.
So the corresponding entries in the output will be missing.

```
//output doesn't contains'?', but may be imperfect
> type input(local-encoding).txt | java -Dfile.encoding=utf-8 -jar ebquery.jar ... > output(utf-8).txt
```

# History

## 0.3.1
### Fixes:
- Improved compatibility of audio tag.

## 0.3.0
### Enhancements:
- Support MonoGraphic, InlineColorGraphic, ColorGraphic and Sound.
- Add new options `-f format` and `-m modules`.

### Notes:
- From this version, the default format of the output changed to `-f text -m ls,ec,tx`.
The default of previous versions is `-f html -m en,sb,sp,in,nb,em,dc,kw,ls,ec,tx`.


## 0.2.1
### Fixes:
- Fixed a fatal issue the program would not work when given no external character map.

## 0.2.0
### Enhancements:
- Support replacement of external character.

## 0.1.0
Initial release.