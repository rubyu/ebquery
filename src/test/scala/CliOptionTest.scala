
package com.github.rubyu.ebquery

import org.specs2.mutable.Specification
import scala.collection.JavaConversions._
import org.kohsuke.args4j.CmdLineParser


class CliOptionTest extends Specification {

  "CliOption" should {
    "have default values" in {
      val option = new CliOption
      val parser = new CmdLineParser(option)
      parser.parseArgument(Nil)
      option.words mustEqual null
      option.dir mustEqual null
      option.help mustEqual false
      option.format mustEqual null
      option.modules mustEqual null
    }

    "have given values" in {
      val args = Array(
        "-d", "dir",
        "--ebmap", "file",
        "-h", "word",
        "-f", "format",
        "-m", "modules")
      val option = new CliOption
      val parser = new CmdLineParser(option)
      parser.parseArgument(args.toList)
      option.words mustEqual Array("word")
      option.dir mustEqual "dir"
      option.ebMap mustEqual "file"
      option.help mustEqual true
      option.format mustEqual "format"
      option.modules mustEqual "modules"
    }

    "have given multiple words" in {
      val args: Array[String] = Array("a", "b")
      val option = new CliOption
      val parser = new CmdLineParser(option)
      parser.parseArgument(args.toList)
      option.words mustEqual Array("a", "b")
    }
  }
}