
package com.github.rubyu.ebquery

import org.specs2.mutable.Specification

class ExternalCharacterMapperTest extends Specification {

  sequential
  
  "parseMappingValue" should {
    "parse EBWin's unicode expression" in {
      val mapper = new ExternalCharacterMapper
      mapper.parseMappingValue("u0041")                   mustEqual "\u0041"
      mapper.parseMappingValue("u0041,u0042")             mustEqual "\u0041\u0042"
      mapper.parseMappingValue("u0041,u0042,u0043")       mustEqual "\u0041\u0042\u0043"
      //unicode strings longer than 4 are incompatible with EBWin
      mapper.parseMappingValue("u0041,u0042,u0043,u0044") mustEqual "\u0041\u0042\u0043\u0044"
    }
    "parse EBWin's unicode expression with uppercase" in {
      val mapper = new ExternalCharacterMapper
      mapper.parseMappingValue("u004A")                   mustEqual "\u004A"
      mapper.parseMappingValue("u004A,u004B")             mustEqual "\u004A\u004B"
      mapper.parseMappingValue("u004A,u004B,u004C")       mustEqual "\u004A\u004B\u004C"
      //unicode strings longer than 4 are incompatible with EBWin
      mapper.parseMappingValue("u004A,u004B,u004C,u004D") mustEqual "\u004A\u004B\u004C\u004D"
    }
    "parse EBWin's unicode expression with lowercase" in {
      val mapper = new ExternalCharacterMapper
      mapper.parseMappingValue("u004a")                   mustEqual "\u004a"
      mapper.parseMappingValue("u004a,u004b")             mustEqual "\u004a\u004b"
      mapper.parseMappingValue("u004a,u004b,u004c")       mustEqual "\u004a\u004b\u004c"
      //unicode strings longer than 4 are incompatible with EBWin
      mapper.parseMappingValue("u004a,u004b,u004c,u004d") mustEqual "\u004a\u004b\u004c\u004d"
    }
  }

  "loadEBMapFile" should {
    "ignore the end of the line comments" in {
      println("ignore the end of the line comments")
      val mapper = new ExternalCharacterMapper
      val map = mapper.loadEBMapFile(new StringInputStream("hA1\tu0041\tA\t#A"), "MS932")
      map("ha1") mustEqual mapper.StringValue("\u0041")
    }

    "ignore comments when keys only" in {
      val mapper = new ExternalCharacterMapper
      val map = mapper.loadEBMapFile(new StringInputStream("#c1\n#c2\n#c3"), "MS932")
      map.size mustEqual 0
    }

    "ignore comments when key and value are available" in {
      val mapper = new ExternalCharacterMapper
      val map = mapper.loadEBMapFile(new StringInputStream("#c1\tv1\n#c2\tv2\n#c3\tv3"), "MS932")
      map.size mustEqual 0
    }

    "ignore comments when key, value and are available" in {
      val mapper = new ExternalCharacterMapper
      val map = mapper.loadEBMapFile(new StringInputStream("#c1\tv1\n#c2\tv2\n#c3\tv3"), "MS932")
      map.size mustEqual 0
    }

    "ignore cases of the prefix of keys" in {
      val mapper = new ExternalCharacterMapper
      val map1 = mapper.loadEBMapFile(new StringInputStream("ha1\tu0041\tA\t#A"), "MS932")
      val map2 = mapper.loadEBMapFile(new StringInputStream("Ha2\tu0042\tB\t#B"), "MS932")
      val map3 = mapper.loadEBMapFile(new StringInputStream("hA3\tu0043\tC\t#C"), "MS932")
      val map4 = mapper.loadEBMapFile(new StringInputStream("HA4\tu0044\tD\t#D"), "MS932")
      map1("ha1") mustEqual mapper.StringValue("\u0041")
      map2("ha2") mustEqual mapper.StringValue("\u0042")
      map3("ha3") mustEqual mapper.StringValue("\u0043")
      map4("ha4") mustEqual mapper.StringValue("\u0044")
    }

    "parse EBWin's map" in {
      val mapper = new ExternalCharacterMapper(getClass.getResource("/KENE7J5.MAP").getPath)
      mapper.map.size mustEqual 519
    }
  }

  "get" should {
    "return StringValue of hankaku Character" in {
      val mapper = new ExternalCharacterMapper(new StringInputStream("hA426\tu00C0\tA\t#À", "utf-8"), "utf-8")
      mapper.getHalfOption(42022) mustEqual Some(mapper.StringValue("\u00C0"))
    }

    "return StringValue of zenkaku Character" in {
      val mapper = new ExternalCharacterMapper(new StringInputStream("zA17E\tu9F76\t\t#齶", "utf-8"), "utf-8")
      mapper.getFullOption(41342) mustEqual Some(mapper.StringValue("\u9F76"))
    }

    "return NullValue" in {
      val mapper = new ExternalCharacterMapper(new StringInputStream("hA121\tnull"))
      mapper.getHalfOption(41249) mustEqual Some(mapper.NullValue)
    }

    "return SkippedValue" in {
      val mapper = new ExternalCharacterMapper(new StringInputStream("hA121\t-"))
      mapper.getHalfOption(41249) mustEqual Some(mapper.SkippedValue)
    }
  }
}