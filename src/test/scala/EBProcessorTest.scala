
package com.github.rubyu.ebquery

import org.specs2.mutable.Specification
import org.specs2.specification.Scope
import fuku.eb4j.Book


class EBProcessorTest extends Specification {

  sequential

  trait KENE7J5 extends Scope {
    val dir = "Z:\\BTSync\\rubyu\\dictionary\\KENE7J5"
    val context = new EBContext(new Book(dir).getSubBook(0), new EBElemTree, new EBProcessor)
  }

  trait OALD7 extends Scope {
    val dir = "Z:\\BTSync\\rubyu\\dictionary\\OALD7"
    val context = new EBContext(new Book(dir).getSubBook(0), new EBElemTree, new EBProcessor)
  }

  "text" should {
    "not escape &" in new KENE7J5 {
      new EBProcessorImpl.text.Text().process(context, "&")
      context.tree.root.toString shouldEqual "&"
    }

    "not escape \"" in new KENE7J5 {
      new EBProcessorImpl.text.Text().process(context, "\"")
      context.tree.root.toString shouldEqual "\""
    }

    "not escape '" in new KENE7J5 {
      new EBProcessorImpl.text.Text().process(context, "'")
      context.tree.root.toString shouldEqual "'"
    }

    "not escape <" in new KENE7J5 {
      new EBProcessorImpl.text.Text().process(context, "<")
      context.tree.root.toString shouldEqual "<"
    }

    "not escape >" in new KENE7J5 {
      new EBProcessorImpl.text.Text().process(context, ">")
      context.tree.root.toString shouldEqual ">"
    }
  }

  "escapedText" should {
    "escape &" in new KENE7J5 {
      new EBProcessorImpl.html.Text().process(context, "&")
      context.tree.root.toString mustEqual "&amp;"
    }

    "escape \"" in new KENE7J5 {
      new EBProcessorImpl.html.Text().process(context, "\"")
      context.tree.root.toString mustEqual "&quot;"
    }

    "escape '" in new KENE7J5 {
      new EBProcessorImpl.html.Text().process(context, "'")
      context.tree.root.toString mustEqual "&#39;"
    }

    "escape <" in new KENE7J5 {
      new EBProcessorImpl.html.Text().process(context, "<")
      context.tree.root.toString mustEqual "&lt;"
    }

    "escape >" in new KENE7J5 {
      new EBProcessorImpl.html.Text().process(context, ">")
      context.tree.root.toString mustEqual "&gt;"
    }
  }

  "invalidCharacter" should {
    "transfer invalid character to EBProcessorImpl.text" in new KENE7J5 {
      context.proc.text = new EBProcessorImpl.text.Text
      new EBProcessorImpl.text.ReplacementCharacter().process(context, EBProcessor.Narrow, 41542)
      context.tree.append("a")
      context.tree.root.toString mustEqual "\uFFFD" + "a"
    }

    "transfer invalid character to DefaultEBProcessor.text" in new KENE7J5 {
      new EBProcessorImpl.text.ReplacementCharacter().process(context, EBProcessor.Narrow, 41542)
      context.tree.append("a")
      context.tree.root.toString mustEqual "a"
    }
  }

  "externalCharacter" should {
    "append img node that has src, alt and class" in new KENE7J5 {
      new EBProcessorImpl.html.ExternalCharacter().process(context, EBProcessor.Narrow, 41542)
      context.tree.append("a")
      context.tree.root.toString mustEqual "<img alt=\"hA246\" class=\"ebec\" src=\"data:image/png;base64," +
        "iVBORw0KGgoAAAANSUhEUgAAAAgAAAAQCAYAAAArij59AAAAPElEQVR42mP4//8/Az6MygFyGcBCWBRgk8QwgaAVlCm" +
        "AuQHdLXBJdN8gsbG4nM4KsOjA9AW6BAqb4pAEAJqSTMKISnbTAAAAAElFTkSuQmCC\">" + "a"
    }
  }


  "subscript" should {
    "open subscript node and close it" in new KENE7J5 {
      new EBProcessorImpl.html.Subscript().preProcess(context)
      context.tree.append("a")
      context.tree.root.toString mustEqual "<span class=\"ebsb\">a</span>"
      context.tree.root ne context.tree.current must beTrue
      new EBProcessorImpl.html.Subscript().postProcess(context)
      context.tree.root eq context.tree.current must beTrue
    }
  }

  "superscript" should {
    "open superscript node and close it" in new KENE7J5 {
      new EBProcessorImpl.html.Superscript().preProcess(context)
      context.tree.append("a")
      context.tree.root.toString mustEqual "<span class=\"ebsp\">a</span>"
      context.tree.root ne context.tree.current must beTrue
      new EBProcessorImpl.html.Superscript().postProcess(context)
      context.tree.root eq context.tree.current must beTrue
    }
  }

  "noNewline" should {
    "open noNewline node and close it" in new KENE7J5 {
      new EBProcessorImpl.html.NoNewline().preProcess(context)
      context.tree.append("a")
      context.tree.root.toString mustEqual "<span class=\"ebnb\">a</span>"
      context.tree.root ne context.tree.current must beTrue
      new EBProcessorImpl.html.NoNewline().postProcess(context)
      context.tree.root eq context.tree.current must beTrue
    }
  }

  "emphasis" should {
    "open emphasis node and close it" in new KENE7J5 {
      new EBProcessorImpl.html.Emphasis().preProcess(context)
      context.tree.append("a")
      context.tree.root.toString mustEqual "<span class=\"ebem\">a</span>"
      context.tree.root ne context.tree.current must beTrue
      new EBProcessorImpl.html.Emphasis().postProcess(context)
      context.tree.root eq context.tree.current must beTrue
    }
  }

  "decoration" should {
    "open italic node and close it" in new KENE7J5 {
      new EBProcessorImpl.html.Decoration().preProcess(context, 0x01)
      context.tree.append("a")
      context.tree.root.toString mustEqual "<span class=\"ebit\">a</span>"
      context.tree.root ne context.tree.current must beTrue
      new EBProcessorImpl.html.Decoration().postProcess(context)
      context.tree.root eq context.tree.current must beTrue
    }

    "open bold node and close it" in new KENE7J5 {
      new EBProcessorImpl.html.Decoration().preProcess(context, 0x03)
      context.tree.append("a")
      context.tree.root.toString mustEqual "<span class=\"ebbo\">a</span>"
      context.tree.root ne context.tree.current must beTrue
      new EBProcessorImpl.html.Decoration().postProcess(context)
      context.tree.root eq context.tree.current must beTrue
    }

    "open underline node and close it" in new KENE7J5 {
      new EBProcessorImpl.html.Decoration().preProcess(context, 0x04)
      context.tree.append("a")
      context.tree.root.toString mustEqual "<span class=\"ebul\">a</span>"
      context.tree.root ne context.tree.current must beTrue
      new EBProcessorImpl.html.Decoration().postProcess(context)
      context.tree.root eq context.tree.current must beTrue
    }
  }

  "keyword" should {
    "open keyword node and close it" in new KENE7J5 {
      new EBProcessorImpl.html.Keyword().preProcess(context)
      context.tree.append("a")
      context.tree.root.toString mustEqual "<span class=\"ebkw\">a</span>"
      context.tree.root ne context.tree.current must beTrue
      new EBProcessorImpl.html.Keyword().postProcess(context)
      context.tree.root eq context.tree.current must beTrue
    }
  }

  "lineSeparator" should {
    "transfer lineSeparator to EBProcessorImpl.text" in new KENE7J5 {
      context.proc.text = new EBProcessorImpl.text.Text
      new EBProcessorImpl.text.Newline().process(context)
      context.tree.append("a")
      context.tree.root.toString mustEqual System.lineSeparator + "a"
    }

    "transfer lineSeparator to DefaultEBProcessor.text" in new KENE7J5 {
      new EBProcessorImpl.text.Newline().process(context)
      context.tree.append("a")
      context.tree.root.toString mustEqual "a"
    }
  }

  "newline" should {
    "append br node" in new KENE7J5 {
      new EBProcessorImpl.html.Newline().process(context)
      context.tree.append("a")
      context.tree.root.toString mustEqual "<br>a"
    }
  }

  "indent" should {
    "open indent node and close it" in new KENE7J5 {
      new EBProcessorImpl.html.Indent().preProcess(context)
      context.tree.append("a")
      context.tree.root.toString mustEqual "<span class=\"ebin\">a</span>"
      context.tree.root ne context.tree.current must beTrue
      new EBProcessorImpl.html.Indent().postProcess(context)
      context.tree.root eq context.tree.current must beTrue
    }
  }

  "ebquery" should {
    "open ebquery node" in new KENE7J5 {
      new EBProcessorImpl.html.Entry().process(context)
      context.tree.append("a")
      context.tree.root.toString mustEqual "<span class=\"ebquery KENE7J5\">a</span>"
    }
  }

  "colorGraphic" should {
    "open img node and close it" in new OALD7 {
      new EBProcessorImpl.html.ColorGraphic().preProcess(context, 1, 49718778)
      context.tree.append("a")
      context.tree.root.toString mustEqual "a"
      context.tree.root ne context.tree.current must beTrue
      new EBProcessorImpl.html.ColorGraphic().postProcess(context)
      context.tree.root eq context.tree.current must beTrue
      context.tree.root.toString.startsWith("a<img alt=\"2F6A5FA\" class=\"ebcg\" src=\"data:image/jpeg;base64,/9j/4AAQS") must beTrue
      context.tree.root.toString.endsWith("srz7so7+7P/2Q==\">") must beTrue
    }
  }

  "sound" should {
    "open audio node and close it" in new OALD7 {
      new EBProcessorImpl.html.Sound().preProcess(context, 1, 265231242, 265233891)
      context.tree.append("a")
      context.tree.root.toString mustEqual "a"
      context.tree.root ne context.tree.current must beTrue
      new EBProcessorImpl.html.Sound().postProcess(context)
      context.tree.root eq context.tree.current must beTrue
      context.tree.root.toString.startsWith("a<audio class=\"ebsd\" controls=\"controls\" src=\"data:audio/x-wav;base64,UklGRl4K") must beTrue
      context.tree.root.toString.endsWith("IOFsRVMQU1FMy45M1VVVVVVVVVVVVVVVQ==\"></audio>") must beTrue
    }
  }

}