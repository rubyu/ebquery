
package com.github.rubyu.ebquery

import org.specs2.mutable.Specification


class EBElemTest extends Specification {

  "EBLeaf" should {
    "equal with a instance be of the same string" in {
      val a = new EBLeaf("a")
      val b = new EBLeaf("a")
      a mustEqual b
    }

    "sort properties asc order in the property's name" in {
      val a = new EBLeaf("a")
      a.property.update("p1", "v")
      a.property.update("p2", "v")
      val b = new EBLeaf("a")
      b.property.update("p2", "v")
      b.property.update("p1", "v")
      a.toString mustEqual "<a p1=\"v\" p2=\"v\">"
      b.toString mustEqual "<a p1=\"v\" p2=\"v\">"
    }
  }

  "EBNode" should {
    "equal with a instance be of the same string" in {
      val a = new EBNode("a")
      val b = new EBNode("a")
      a.toString mustEqual b.toString
    }

    "sort properties asc order in the property's name" in {
      val a = new EBNode("a")
      a.property.update("p1", "v")
      a.property.update("p2", "v")
      val b = new EBNode("a")
      b.property.update("p2", "v")
      b.property.update("p1", "v")
      a.toString mustEqual "<a p1=\"v\" p2=\"v\"></a>"
      b.toString mustEqual "<a p1=\"v\" p2=\"v\"></a>"
    }

    "output children in the middle position" in {
      val a = new EBNode("a")
      a.children += "c"
      a.toString mustEqual "<a>c</a>"
    }
  }

  "EBTransparentNode" should {
    "output only children" in {
      val a = new EBTransparentNode
      a.children += "c"
      a.toString mustEqual "c"
    }
  }

  "EBElemTree" should {
    "open a EBElemTree" in {
      val tree = new EBElemTree
      val a = new EBLeaf("a")
      tree.open(a)
      tree.root.toString mustEqual "<a>"
    }

    "open a EBElemTree and close" in {
      val tree = new EBElemTree
      val a = new EBLeaf("a")
      tree.open(a)
      tree.close()
      tree.root.toString mustEqual "<a>"
    }

    "clear children" in {
      val tree = new EBElemTree
      val a = new EBLeaf("a")
      tree.open(a)
      tree.close()
      tree.root.toString mustEqual "<a>"
      tree.clear()
      tree.root.toString mustEqual ""
    }

    "throw an IllegalStateException when open an EBLeaf" in {
      val tree = new EBElemTree
      val a = new EBLeaf("a")
      val b = new EBLeaf("b")
      tree.open(a)
      tree.open(b) must throwAn(new IllegalStateException())
    }

    "throw an IllegalStateException when close the root element" in {
      val tree = new EBElemTree
      tree.close() must throwAn(new IllegalStateException())
    }
  }

  "EBMonoGraphicNode" should {
    "return only child when have no src property" in {
      val tree = new EBElemTree
      val node = new MonoGraphicNode(0, 0)
      tree.open(node)
      tree.append("a")
      tree.root.toString mustEqual "a"
    }

    "return content" in {
      val tree = new EBElemTree
      val node = new MonoGraphicNode(0, 0)
      node.property.update("src", "dummy")
      tree.open(node)
      tree.root.toString mustEqual "<img src=\"dummy\">"
    }

    "return content with childnen" in {
      val tree = new EBElemTree
      val node = new MonoGraphicNode(0, 0)
      node.property.update("src", "dummy")
      tree.open(node)
      tree.append("a")
      tree.root.toString mustEqual "a<img src=\"dummy\">"
    }

    //todo open and close a node
  }
}
