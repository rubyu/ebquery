
package com.github.rubyu.ebquery

import org.specs2.mutable.Specification
import org.specs2.specification.Scope
import java.io._
import fuku.eb4j.Book


class EBProcessorAdapterTest extends Specification {

  sequential

  trait scope extends Scope {
    val dir = "Z:\\BTSync\\rubyu\\dictionary\\KENE7J5"
    val dic = new Book(dir).getSubBook(0)
    val proc = new EBProcessor
  }

  "adapter" should {
    "write hankaku external character strings" in new scope {
      val mapper = new ExternalCharacterMapper(new StringInputStream("hA426\tu00C0"))
      val adapter = new EBProcessorAdapter(dic, mapper, proc)
      proc.text = new EBProcessorImpl.html.Text
      adapter.narrow = true
      adapter.append(42022)
      adapter.getObject mustEqual "\u00C0"
    }

    "write zenkaku external character strings" in new scope {
      val mapper = new ExternalCharacterMapper(new StringInputStream("zA426\tu00C0"))
      val adapter = new EBProcessorAdapter(dic, mapper, proc)
      proc.text = new EBProcessorImpl.html.Text
      adapter.narrow = false
      adapter.append(42022)
      adapter.getObject mustEqual "\u00C0"
    }

    "skip when value is NullValue" in new scope {
      val mapper = new ExternalCharacterMapper(new StringInputStream("hA426\tnull"))
      val adapter = new EBProcessorAdapter(dic, mapper, proc)
      proc.text = new EBProcessorImpl.html.Text
      adapter.narrow = true
      adapter.append(42022)
      adapter.getObject mustEqual ""
    }

    "write img tag when value is SkippedValue" in new scope {
      val mapper = new ExternalCharacterMapper(new StringInputStream("hA246\t-"))
      val adapter = new EBProcessorAdapter(dic, mapper, proc)
      proc.text = new EBProcessorImpl.html.Text
      proc.externalCharacter = new EBProcessorImpl.html.ExternalCharacter
      adapter.narrow = true
      adapter.append(41542)
      adapter.getObject mustEqual "<img alt=\"hA246\" class=\"ebec\" src=\"data:image/png;base64," +
        "iVBORw0KGgoAAAANSUhEUgAAAAgAAAAQCAYAAAArij59AAAAPElEQVR42mP4//8/Az6MygFyGcBCWBRgk8Qw" +
        "gaAVlCmAuQHdLXBJdN8gsbG4nM4KsOjA9AW6BAqb4pAEAJqSTMKISnbTAAAAAElFTkSuQmCC\">"
    }

    "call EBProcessor.indent 0 times" in new scope {
      val adapter = new EBProcessorAdapter(dic, new ExternalCharacterMapper, proc)
      proc.indent = new EBProcessorImpl.html.Indent
      adapter.getObject mustEqual ""
      adapter.setIndent(0)
      adapter.getObject mustEqual ""
    }

    "call EBProcessor.indent 1 times" in new scope {
      val adapter = new EBProcessorAdapter(dic, new ExternalCharacterMapper, proc)
      proc.indent = new EBProcessorImpl.html.Indent
      adapter.getObject mustEqual ""
      adapter.setIndent(1)
      adapter.getObject mustEqual "<span class=\"ebin\"></span>"
    }

    "call EBProcessor.indent 2 times" in new scope {
      val adapter = new EBProcessorAdapter(dic, new ExternalCharacterMapper, proc)
      proc.indent = new EBProcessorImpl.html.Indent
      adapter.getObject mustEqual ""
      adapter.setIndent(2)
      adapter.getObject mustEqual "<span class=\"ebin\"><span class=\"ebin\"></span></span>"
    }

    "clear context" in new scope {
      val adapter = new EBProcessorAdapter(dic, new ExternalCharacterMapper, proc)
      proc.text = new EBProcessorImpl.html.Text
      adapter.getObject mustEqual ""
      adapter.append("a")
      adapter.getObject mustEqual "a"
      adapter.clear()
      adapter.getObject mustEqual ""
    }

    "clear indent" in new scope {
      val adapter = new EBProcessorAdapter(dic, new ExternalCharacterMapper, proc)
      proc.indent = new EBProcessorImpl.html.Indent
      adapter.indent mustEqual 0
      adapter.setIndent(1)
      adapter.indent mustEqual 1
      adapter.clear()
      adapter.indent mustEqual 0
    }
  }
}