
package com.github.rubyu.ebquery

import org.specs2.mutable.{Specification, After}
import org.specs2.specification.Scope
import java.io._
import com.rexsl.w3c.ValidatorBuilder


class MainTest extends Specification {

  sequential

  class AttemptToExitException(val status: Int) extends RuntimeException

  class MockExitSecurityManager extends java.rmi.RMISecurityManager {
    override def checkExit(status: Int) { throw new AttemptToExitException(status) }
    override def checkPermission(perm: java.security.Permission) {}
  }

  trait scope extends Scope with After {
    val _sm = System.getSecurityManager
    val _stdout = System.out
    val _stderr = System.err
    val _stdin = System.in

    val stdout, stderr = new ByteArrayOutputStream

    System.setSecurityManager(new MockExitSecurityManager)
    System.setOut(new PrintStream(new BufferedOutputStream(stdout), true, "utf-8"))
    System.setErr(new PrintStream(new BufferedOutputStream(stderr), true, "utf-8"))

    def after {
      System.setSecurityManager(_sm)
      System.setOut(_stdout)
      System.setErr(_stderr)
      System.setIn(_stdin)
    }
  }

  "getWordsFromSTDIN" should {

    sequential

    "return empty Array when STDIN is empty" in {
      Main.getWordsFromSTDIN() mustEqual(Array())
    }

    "return given words" in new scope {
      System.setIn(new ByteArrayInputStream("word1\nword2".getBytes))
      Main.getWordsFromSTDIN() mustEqual(Array("word1", "word2"))
    }
  }

  "printResult" should {
    val dir = "Z:\\BTSync\\rubyu\\dictionary\\KENE7J5"

    "output valid html contents" in {
      val proc = new EBProcessor
      proc.entry = new EBProcessorImpl.html.Entry
      proc.subscript = new EBProcessorImpl.html.Subscript
      proc.superscript = new EBProcessorImpl.html.Superscript
      proc.indent = new EBProcessorImpl.html.Indent
      proc.noNewline = new EBProcessorImpl.html.NoNewline
      proc.emphasis = new EBProcessorImpl.html.Emphasis
      proc.decoration = new EBProcessorImpl.html.Decoration
      proc.keyword = new EBProcessorImpl.html.Keyword
      proc.newline = new EBProcessorImpl.html.Newline
      proc.externalCharacter = new EBProcessorImpl.html.ExternalCharacter
      proc.monoGraphic = new EBProcessorImpl.html.MonoGraphic
      proc.colorGraphic = new EBProcessorImpl.html.ColorGraphic
      proc.sound = new EBProcessorImpl.html.Sound
      proc.text = new EBProcessorImpl.html.Text

      val stream = new ByteArrayOutputStream
      val ps = new PrintStream(stream, true, "utf-8")
      ps.println("<!DOCTYPE html><html><head>")
      ps.println("<meta charset=\"utf-8\">")
      ps.println("<title>printResultTest</title>")
      ps.println("</head><body>")
      Main.printResult(ps, dir, List("angel"), new ExternalCharacterMapper, proc)
      ps.println("</body></html>")
      val html = stream.toString("utf-8")
      println(html)
      val response = new ValidatorBuilder().html().validate(html)
      response.valid must beTrue
    }
  }

  "main" should {

    sequential

    "output help messages to stdout and exit with code(0)"+
      "when '-h' option was given" in new scope {
      Main.main(Array("-h")) must throwAn(new AttemptToExitException(0))
      System.out.flush()
      stdout.toString mustEqual(List(
        "NAME",
        " qbquery -- Command Line Interface for Epwing(Electronic Publishing-WING)",
        "",
        "SYNOPSIS",
        " java -jar ebquery.jar [-h | --help]",
        " java -jar ebquery.jar [-f format] [-m modules] [--ebmap file] -d dir word [word ...]",
        "",
        "DESCRIPTION",
        "Executes a query for an Epwing dictionary, and prints results to standard output. " +
          "The options are as follows:",
        "",
        " -d dir       : Path to the directory containing Epwing's CATALOGS file.",
        " words        : Query words.",
        " -f format    : Output format. The default value is 'text'." +
          "Allowed values are 'text' and 'html'.",
        " -m modules   : Modules used for rendering the output. " +
          "The default value is 'tx,ec,ls'. " +
          "Modules are en, sb, sp, in, nb, em, dc, kw, ls, ec, mg, cg, sd and tx.",
        " --ebmap file : Path to the EBWin's GAIJI mapping file.",
        " -h (--help)  : Print help.",
        "",
        " If no word is specified, then the standard input is read.",
        "").mkString(System.lineSeparator))
    }

    "output error messages to stderr and exit with code(1) "+
      "when '-d' option is missing" in new scope {
      Main.main(Array()) must throwAn(new AttemptToExitException(1))
      System.err.flush()
      stderr.toString mustEqual(
        List(
          "option '-d' is required",
          "").mkString(System.lineSeparator))
    }

    "output error messages to stderr and exit with code(1) "+
      "when both 'words' and STDIN are missing" in new scope {
      Main.main(Array("-d", "")) must throwAn(new AttemptToExitException(1))
      System.err.flush()
      stderr.toString mustEqual(
        List(
          "'words' or STDIN are required",
          "").mkString(System.lineSeparator))
    }

    "output results without ebmap" in new scope {
      Main.main(Array(
        "-d", "Z:\\BTSync\\rubyu\\dictionary\\KENE7J5",
        "-f", "html",
        "-m", "tx,ec",
        "get")) must throwAn(new AttemptToExitException(0))
      System.out.flush()
      val result = stdout.toString("utf-8")
      println(result)
      result.indexOf("g<img alt=\"hA246\" class=\"ebec\" src=\"data:image/png;base64,iVBORw0KGgoAAAAN" +
        "SUhEUgAAAAgAAAAQCAYAAAArij59AAAAPElEQVR42mP4//8/Az6MygFyGcBCWBRgk8QwgaAVlCmAuQHdLXBJdN8gsb" +
        "G4nM4KsOjA9AW6BAqb4pAEAJqSTMKISnbTAAAAAElFTkSuQmCC\">t") mustNotEqual -1
    }

    "output results with ebmap" in new scope {
      Main.main(Array(
        "-d", "Z:\\BTSync\\rubyu\\dictionary\\KENE7J5",
        "-f", "html",
        "-m", "tx,ec",
        "--ebmap", getClass.getResource("/KENE7J5.MAP").getPath,
        "get")) must throwAn(new AttemptToExitException(0))
      System.out.flush()
      val result = stdout.toString("utf-8")
      result.indexOf("gɑ́t") mustNotEqual -1
    }

    //todo test actual output of all modules
  }

  "getProcessorForText" should {
    "return proc that has default" in {
      val proc = Main.getProcessorForText(null)
      proc.newline.isInstanceOf[EBProcessorImpl.text.Newline] must beTrue
      proc.externalCharacter.isInstanceOf[EBProcessorImpl.text.ReplacementCharacter] must beTrue
      proc.text.isInstanceOf[EBProcessorImpl.text.Text] must beTrue
    }

    "return proc that has instance of text.Newline" in {
      val proc = Main.getProcessorForText("ls")
      proc.newline.isInstanceOf[EBProcessorImpl.text.Newline] must beTrue
      proc.externalCharacter.isInstanceOf[EBProcessorImpl.nop.ExternalCharacter] must beTrue
      proc.text.isInstanceOf[EBProcessorImpl.nop.Text] must beTrue
    }

    "return proc that has instance of text.ReplacementCharacter" in {
      val proc = Main.getProcessorForText("ec")
      proc.newline.isInstanceOf[EBProcessorImpl.nop.Newline] must beTrue
      proc.externalCharacter.isInstanceOf[EBProcessorImpl.text.ReplacementCharacter] must beTrue
      proc.text.isInstanceOf[EBProcessorImpl.nop.Text] must beTrue
    }

    "return proc that has instance of text.Text" in {
      val proc = Main.getProcessorForText("tx")
      proc.newline.isInstanceOf[EBProcessorImpl.nop.Newline] must beTrue
      proc.externalCharacter.isInstanceOf[EBProcessorImpl.nop.ExternalCharacter] must beTrue
      proc.text.isInstanceOf[EBProcessorImpl.text.Text] must beTrue
    }
  }

  "getProcessorForHtml" should {
    "return proc that has default" in {
      val proc = Main.getProcessorForHtml(null)
      proc.entry.isInstanceOf[EBProcessorImpl.nop.Entry] must beTrue
      proc.subscript.isInstanceOf[EBProcessorImpl.nop.Subscript] must beTrue
      proc.superscript.isInstanceOf[EBProcessorImpl.nop.Superscript] must beTrue
      proc.indent.isInstanceOf[EBProcessorImpl.nop.Indent] must beTrue
      proc.noNewline.isInstanceOf[EBProcessorImpl.nop.NoNewline] must beTrue
      proc.emphasis.isInstanceOf[EBProcessorImpl.nop.Emphasis] must beTrue
      proc.decoration.isInstanceOf[EBProcessorImpl.nop.Decoration] must beTrue
      proc.keyword.isInstanceOf[EBProcessorImpl.nop.Keyword] must beTrue
      proc.newline.isInstanceOf[EBProcessorImpl.html.Newline] must beTrue
      proc.externalCharacter.isInstanceOf[EBProcessorImpl.html.ExternalCharacter] must beTrue
      proc.monoGraphic.isInstanceOf[EBProcessorImpl.nop.MonoGraphic] must beTrue
      proc.colorGraphic.isInstanceOf[EBProcessorImpl.nop.ColorGraphic] must beTrue
      proc.sound.isInstanceOf[EBProcessorImpl.nop.Sound] must beTrue
      proc.text.isInstanceOf[EBProcessorImpl.html.Text] must beTrue
    }

    "return proc that has instance of html.Entry" in {
      val proc = Main.getProcessorForHtml("en")
      proc.entry.isInstanceOf[EBProcessorImpl.html.Entry] must beTrue
      proc.subscript.isInstanceOf[EBProcessorImpl.nop.Subscript] must beTrue
      proc.superscript.isInstanceOf[EBProcessorImpl.nop.Superscript] must beTrue
      proc.indent.isInstanceOf[EBProcessorImpl.nop.Indent] must beTrue
      proc.noNewline.isInstanceOf[EBProcessorImpl.nop.NoNewline] must beTrue
      proc.emphasis.isInstanceOf[EBProcessorImpl.nop.Emphasis] must beTrue
      proc.decoration.isInstanceOf[EBProcessorImpl.nop.Decoration] must beTrue
      proc.keyword.isInstanceOf[EBProcessorImpl.nop.Keyword] must beTrue
      proc.newline.isInstanceOf[EBProcessorImpl.nop.Newline] must beTrue
      proc.externalCharacter.isInstanceOf[EBProcessorImpl.nop.ExternalCharacter] must beTrue
      proc.monoGraphic.isInstanceOf[EBProcessorImpl.nop.MonoGraphic] must beTrue
      proc.colorGraphic.isInstanceOf[EBProcessorImpl.nop.ColorGraphic] must beTrue
      proc.sound.isInstanceOf[EBProcessorImpl.nop.Sound] must beTrue
      proc.text.isInstanceOf[EBProcessorImpl.nop.Text] must beTrue
    }

    "return proc that has instance of html.Subscript" in {
      val proc = Main.getProcessorForHtml("sb")
      proc.entry.isInstanceOf[EBProcessorImpl.nop.Entry] must beTrue
      proc.subscript.isInstanceOf[EBProcessorImpl.html.Subscript] must beTrue
      proc.superscript.isInstanceOf[EBProcessorImpl.nop.Superscript] must beTrue
      proc.indent.isInstanceOf[EBProcessorImpl.nop.Indent] must beTrue
      proc.noNewline.isInstanceOf[EBProcessorImpl.nop.NoNewline] must beTrue
      proc.emphasis.isInstanceOf[EBProcessorImpl.nop.Emphasis] must beTrue
      proc.decoration.isInstanceOf[EBProcessorImpl.nop.Decoration] must beTrue
      proc.keyword.isInstanceOf[EBProcessorImpl.nop.Keyword] must beTrue
      proc.newline.isInstanceOf[EBProcessorImpl.nop.Newline] must beTrue
      proc.externalCharacter.isInstanceOf[EBProcessorImpl.nop.ExternalCharacter] must beTrue
      proc.monoGraphic.isInstanceOf[EBProcessorImpl.nop.MonoGraphic] must beTrue
      proc.colorGraphic.isInstanceOf[EBProcessorImpl.nop.ColorGraphic] must beTrue
      proc.sound.isInstanceOf[EBProcessorImpl.nop.Sound] must beTrue
      proc.text.isInstanceOf[EBProcessorImpl.nop.Text] must beTrue
    }

    "return proc that has instance of html.Superscript" in {
      val proc = Main.getProcessorForHtml("sp")
      proc.entry.isInstanceOf[EBProcessorImpl.nop.Entry] must beTrue
      proc.subscript.isInstanceOf[EBProcessorImpl.nop.Subscript] must beTrue
      proc.superscript.isInstanceOf[EBProcessorImpl.html.Superscript] must beTrue
      proc.indent.isInstanceOf[EBProcessorImpl.nop.Indent] must beTrue
      proc.noNewline.isInstanceOf[EBProcessorImpl.nop.NoNewline] must beTrue
      proc.emphasis.isInstanceOf[EBProcessorImpl.nop.Emphasis] must beTrue
      proc.decoration.isInstanceOf[EBProcessorImpl.nop.Decoration] must beTrue
      proc.keyword.isInstanceOf[EBProcessorImpl.nop.Keyword] must beTrue
      proc.newline.isInstanceOf[EBProcessorImpl.nop.Newline] must beTrue
      proc.externalCharacter.isInstanceOf[EBProcessorImpl.nop.ExternalCharacter] must beTrue
      proc.monoGraphic.isInstanceOf[EBProcessorImpl.nop.MonoGraphic] must beTrue
      proc.colorGraphic.isInstanceOf[EBProcessorImpl.nop.ColorGraphic] must beTrue
      proc.sound.isInstanceOf[EBProcessorImpl.nop.Sound] must beTrue
      proc.text.isInstanceOf[EBProcessorImpl.nop.Text] must beTrue
    }

    "return proc that has instance of html.Indent" in {
      val proc = Main.getProcessorForHtml("in")
      proc.entry.isInstanceOf[EBProcessorImpl.nop.Entry] must beTrue
      proc.subscript.isInstanceOf[EBProcessorImpl.nop.Subscript] must beTrue
      proc.superscript.isInstanceOf[EBProcessorImpl.nop.Superscript] must beTrue
      proc.indent.isInstanceOf[EBProcessorImpl.html.Indent] must beTrue
      proc.noNewline.isInstanceOf[EBProcessorImpl.nop.NoNewline] must beTrue
      proc.emphasis.isInstanceOf[EBProcessorImpl.nop.Emphasis] must beTrue
      proc.decoration.isInstanceOf[EBProcessorImpl.nop.Decoration] must beTrue
      proc.keyword.isInstanceOf[EBProcessorImpl.nop.Keyword] must beTrue
      proc.newline.isInstanceOf[EBProcessorImpl.nop.Newline] must beTrue
      proc.externalCharacter.isInstanceOf[EBProcessorImpl.nop.ExternalCharacter] must beTrue
      proc.monoGraphic.isInstanceOf[EBProcessorImpl.nop.MonoGraphic] must beTrue
      proc.colorGraphic.isInstanceOf[EBProcessorImpl.nop.ColorGraphic] must beTrue
      proc.sound.isInstanceOf[EBProcessorImpl.nop.Sound] must beTrue
      proc.text.isInstanceOf[EBProcessorImpl.nop.Text] must beTrue
    }

    "return proc that has instance of html.NoNewline" in {
      val proc = Main.getProcessorForHtml("nb")
      proc.entry.isInstanceOf[EBProcessorImpl.nop.Entry] must beTrue
      proc.subscript.isInstanceOf[EBProcessorImpl.nop.Subscript] must beTrue
      proc.superscript.isInstanceOf[EBProcessorImpl.nop.Superscript] must beTrue
      proc.indent.isInstanceOf[EBProcessorImpl.nop.Indent] must beTrue
      proc.noNewline.isInstanceOf[EBProcessorImpl.html.NoNewline] must beTrue
      proc.emphasis.isInstanceOf[EBProcessorImpl.nop.Emphasis] must beTrue
      proc.decoration.isInstanceOf[EBProcessorImpl.nop.Decoration] must beTrue
      proc.keyword.isInstanceOf[EBProcessorImpl.nop.Keyword] must beTrue
      proc.newline.isInstanceOf[EBProcessorImpl.nop.Newline] must beTrue
      proc.externalCharacter.isInstanceOf[EBProcessorImpl.nop.ExternalCharacter] must beTrue
      proc.monoGraphic.isInstanceOf[EBProcessorImpl.nop.MonoGraphic] must beTrue
      proc.colorGraphic.isInstanceOf[EBProcessorImpl.nop.ColorGraphic] must beTrue
      proc.sound.isInstanceOf[EBProcessorImpl.nop.Sound] must beTrue
      proc.text.isInstanceOf[EBProcessorImpl.nop.Text] must beTrue
    }

    "return proc that has instance of html.Emphasis" in {
      val proc = Main.getProcessorForHtml("em")
      proc.entry.isInstanceOf[EBProcessorImpl.nop.Entry] must beTrue
      proc.subscript.isInstanceOf[EBProcessorImpl.nop.Subscript] must beTrue
      proc.superscript.isInstanceOf[EBProcessorImpl.nop.Superscript] must beTrue
      proc.indent.isInstanceOf[EBProcessorImpl.nop.Indent] must beTrue
      proc.noNewline.isInstanceOf[EBProcessorImpl.nop.NoNewline] must beTrue
      proc.emphasis.isInstanceOf[EBProcessorImpl.html.Emphasis] must beTrue
      proc.decoration.isInstanceOf[EBProcessorImpl.nop.Decoration] must beTrue
      proc.keyword.isInstanceOf[EBProcessorImpl.nop.Keyword] must beTrue
      proc.newline.isInstanceOf[EBProcessorImpl.nop.Newline] must beTrue
      proc.externalCharacter.isInstanceOf[EBProcessorImpl.nop.ExternalCharacter] must beTrue
      proc.monoGraphic.isInstanceOf[EBProcessorImpl.nop.MonoGraphic] must beTrue
      proc.colorGraphic.isInstanceOf[EBProcessorImpl.nop.ColorGraphic] must beTrue
      proc.sound.isInstanceOf[EBProcessorImpl.nop.Sound] must beTrue
      proc.text.isInstanceOf[EBProcessorImpl.nop.Text] must beTrue
    }

    "return proc that has instance of html.Decoration" in {
      val proc = Main.getProcessorForHtml("dc")
      proc.entry.isInstanceOf[EBProcessorImpl.nop.Entry] must beTrue
      proc.subscript.isInstanceOf[EBProcessorImpl.nop.Subscript] must beTrue
      proc.superscript.isInstanceOf[EBProcessorImpl.nop.Superscript] must beTrue
      proc.indent.isInstanceOf[EBProcessorImpl.nop.Indent] must beTrue
      proc.noNewline.isInstanceOf[EBProcessorImpl.nop.NoNewline] must beTrue
      proc.emphasis.isInstanceOf[EBProcessorImpl.nop.Emphasis] must beTrue
      proc.decoration.isInstanceOf[EBProcessorImpl.html.Decoration] must beTrue
      proc.keyword.isInstanceOf[EBProcessorImpl.nop.Keyword] must beTrue
      proc.newline.isInstanceOf[EBProcessorImpl.nop.Newline] must beTrue
      proc.externalCharacter.isInstanceOf[EBProcessorImpl.nop.ExternalCharacter] must beTrue
      proc.monoGraphic.isInstanceOf[EBProcessorImpl.nop.MonoGraphic] must beTrue
      proc.colorGraphic.isInstanceOf[EBProcessorImpl.nop.ColorGraphic] must beTrue
      proc.sound.isInstanceOf[EBProcessorImpl.nop.Sound] must beTrue
      proc.text.isInstanceOf[EBProcessorImpl.nop.Text] must beTrue
    }

    "return proc that has instance of html.Keyword" in {
      val proc = Main.getProcessorForHtml("kw")
      proc.entry.isInstanceOf[EBProcessorImpl.nop.Entry] must beTrue
      proc.subscript.isInstanceOf[EBProcessorImpl.nop.Subscript] must beTrue
      proc.superscript.isInstanceOf[EBProcessorImpl.nop.Superscript] must beTrue
      proc.indent.isInstanceOf[EBProcessorImpl.nop.Indent] must beTrue
      proc.noNewline.isInstanceOf[EBProcessorImpl.nop.NoNewline] must beTrue
      proc.emphasis.isInstanceOf[EBProcessorImpl.nop.Emphasis] must beTrue
      proc.decoration.isInstanceOf[EBProcessorImpl.nop.Decoration] must beTrue
      proc.keyword.isInstanceOf[EBProcessorImpl.html.Keyword] must beTrue
      proc.newline.isInstanceOf[EBProcessorImpl.nop.Newline] must beTrue
      proc.externalCharacter.isInstanceOf[EBProcessorImpl.nop.ExternalCharacter] must beTrue
      proc.monoGraphic.isInstanceOf[EBProcessorImpl.nop.MonoGraphic] must beTrue
      proc.colorGraphic.isInstanceOf[EBProcessorImpl.nop.ColorGraphic] must beTrue
      proc.sound.isInstanceOf[EBProcessorImpl.nop.Sound] must beTrue
      proc.text.isInstanceOf[EBProcessorImpl.nop.Text] must beTrue
    }

    "return proc that has instance of html.Newline" in {
      val proc = Main.getProcessorForHtml("ls")
      proc.entry.isInstanceOf[EBProcessorImpl.nop.Entry] must beTrue
      proc.subscript.isInstanceOf[EBProcessorImpl.nop.Subscript] must beTrue
      proc.superscript.isInstanceOf[EBProcessorImpl.nop.Superscript] must beTrue
      proc.indent.isInstanceOf[EBProcessorImpl.nop.Indent] must beTrue
      proc.noNewline.isInstanceOf[EBProcessorImpl.nop.NoNewline] must beTrue
      proc.emphasis.isInstanceOf[EBProcessorImpl.nop.Emphasis] must beTrue
      proc.decoration.isInstanceOf[EBProcessorImpl.nop.Decoration] must beTrue
      proc.keyword.isInstanceOf[EBProcessorImpl.nop.Keyword] must beTrue
      proc.newline.isInstanceOf[EBProcessorImpl.html.Newline] must beTrue
      proc.externalCharacter.isInstanceOf[EBProcessorImpl.nop.ExternalCharacter] must beTrue
      proc.monoGraphic.isInstanceOf[EBProcessorImpl.nop.MonoGraphic] must beTrue
      proc.colorGraphic.isInstanceOf[EBProcessorImpl.nop.ColorGraphic] must beTrue
      proc.sound.isInstanceOf[EBProcessorImpl.nop.Sound] must beTrue
      proc.text.isInstanceOf[EBProcessorImpl.nop.Text] must beTrue
    }

    "return proc that has instance of html.ExternalCharacter" in {
      val proc = Main.getProcessorForHtml("ec")
      proc.entry.isInstanceOf[EBProcessorImpl.nop.Entry] must beTrue
      proc.subscript.isInstanceOf[EBProcessorImpl.nop.Subscript] must beTrue
      proc.superscript.isInstanceOf[EBProcessorImpl.nop.Superscript] must beTrue
      proc.indent.isInstanceOf[EBProcessorImpl.nop.Indent] must beTrue
      proc.noNewline.isInstanceOf[EBProcessorImpl.nop.NoNewline] must beTrue
      proc.emphasis.isInstanceOf[EBProcessorImpl.nop.Emphasis] must beTrue
      proc.decoration.isInstanceOf[EBProcessorImpl.nop.Decoration] must beTrue
      proc.keyword.isInstanceOf[EBProcessorImpl.nop.Keyword] must beTrue
      proc.newline.isInstanceOf[EBProcessorImpl.nop.Newline] must beTrue
      proc.externalCharacter.isInstanceOf[EBProcessorImpl.html.ExternalCharacter] must beTrue
      proc.monoGraphic.isInstanceOf[EBProcessorImpl.nop.MonoGraphic] must beTrue
      proc.colorGraphic.isInstanceOf[EBProcessorImpl.nop.ColorGraphic] must beTrue
      proc.sound.isInstanceOf[EBProcessorImpl.nop.Sound] must beTrue
      proc.text.isInstanceOf[EBProcessorImpl.nop.Text] must beTrue
    }

    "return proc that has instance of html.MonoGraphic" in {
      val proc = Main.getProcessorForHtml("mg")
      proc.entry.isInstanceOf[EBProcessorImpl.nop.Entry] must beTrue
      proc.subscript.isInstanceOf[EBProcessorImpl.nop.Subscript] must beTrue
      proc.superscript.isInstanceOf[EBProcessorImpl.nop.Superscript] must beTrue
      proc.indent.isInstanceOf[EBProcessorImpl.nop.Indent] must beTrue
      proc.noNewline.isInstanceOf[EBProcessorImpl.nop.NoNewline] must beTrue
      proc.emphasis.isInstanceOf[EBProcessorImpl.nop.Emphasis] must beTrue
      proc.decoration.isInstanceOf[EBProcessorImpl.nop.Decoration] must beTrue
      proc.keyword.isInstanceOf[EBProcessorImpl.nop.Keyword] must beTrue
      proc.newline.isInstanceOf[EBProcessorImpl.nop.Newline] must beTrue
      proc.externalCharacter.isInstanceOf[EBProcessorImpl.nop.ExternalCharacter] must beTrue
      proc.monoGraphic.isInstanceOf[EBProcessorImpl.html.MonoGraphic] must beTrue
      proc.colorGraphic.isInstanceOf[EBProcessorImpl.nop.ColorGraphic] must beTrue
      proc.sound.isInstanceOf[EBProcessorImpl.nop.Sound] must beTrue
      proc.text.isInstanceOf[EBProcessorImpl.nop.Text] must beTrue
    }

    "return proc that has instance of html.ColorGraphic" in {
      val proc = Main.getProcessorForHtml("cg")
      proc.entry.isInstanceOf[EBProcessorImpl.nop.Entry] must beTrue
      proc.subscript.isInstanceOf[EBProcessorImpl.nop.Subscript] must beTrue
      proc.superscript.isInstanceOf[EBProcessorImpl.nop.Superscript] must beTrue
      proc.indent.isInstanceOf[EBProcessorImpl.nop.Indent] must beTrue
      proc.noNewline.isInstanceOf[EBProcessorImpl.nop.NoNewline] must beTrue
      proc.emphasis.isInstanceOf[EBProcessorImpl.nop.Emphasis] must beTrue
      proc.decoration.isInstanceOf[EBProcessorImpl.nop.Decoration] must beTrue
      proc.keyword.isInstanceOf[EBProcessorImpl.nop.Keyword] must beTrue
      proc.newline.isInstanceOf[EBProcessorImpl.nop.Newline] must beTrue
      proc.externalCharacter.isInstanceOf[EBProcessorImpl.nop.ExternalCharacter] must beTrue
      proc.monoGraphic.isInstanceOf[EBProcessorImpl.nop.MonoGraphic] must beTrue
      proc.colorGraphic.isInstanceOf[EBProcessorImpl.html.ColorGraphic] must beTrue
      proc.sound.isInstanceOf[EBProcessorImpl.nop.Sound] must beTrue
      proc.text.isInstanceOf[EBProcessorImpl.nop.Text] must beTrue
    }

    "return proc that has instance of html.Sound" in {
      val proc = Main.getProcessorForHtml("sd")
      proc.entry.isInstanceOf[EBProcessorImpl.nop.Entry] must beTrue
      proc.subscript.isInstanceOf[EBProcessorImpl.nop.Subscript] must beTrue
      proc.superscript.isInstanceOf[EBProcessorImpl.nop.Superscript] must beTrue
      proc.indent.isInstanceOf[EBProcessorImpl.nop.Indent] must beTrue
      proc.noNewline.isInstanceOf[EBProcessorImpl.nop.NoNewline] must beTrue
      proc.emphasis.isInstanceOf[EBProcessorImpl.nop.Emphasis] must beTrue
      proc.decoration.isInstanceOf[EBProcessorImpl.nop.Decoration] must beTrue
      proc.keyword.isInstanceOf[EBProcessorImpl.nop.Keyword] must beTrue
      proc.newline.isInstanceOf[EBProcessorImpl.nop.Newline] must beTrue
      proc.externalCharacter.isInstanceOf[EBProcessorImpl.nop.ExternalCharacter] must beTrue
      proc.monoGraphic.isInstanceOf[EBProcessorImpl.nop.MonoGraphic] must beTrue
      proc.colorGraphic.isInstanceOf[EBProcessorImpl.nop.ColorGraphic] must beTrue
      proc.sound.isInstanceOf[EBProcessorImpl.html.Sound] must beTrue
      proc.text.isInstanceOf[EBProcessorImpl.nop.Text] must beTrue
    }

    "return proc that has instance of html.Text" in {
      val proc = Main.getProcessorForHtml("tx")
      proc.entry.isInstanceOf[EBProcessorImpl.nop.Entry] must beTrue
      proc.subscript.isInstanceOf[EBProcessorImpl.nop.Subscript] must beTrue
      proc.superscript.isInstanceOf[EBProcessorImpl.nop.Superscript] must beTrue
      proc.indent.isInstanceOf[EBProcessorImpl.nop.Indent] must beTrue
      proc.noNewline.isInstanceOf[EBProcessorImpl.nop.NoNewline] must beTrue
      proc.emphasis.isInstanceOf[EBProcessorImpl.nop.Emphasis] must beTrue
      proc.decoration.isInstanceOf[EBProcessorImpl.nop.Decoration] must beTrue
      proc.keyword.isInstanceOf[EBProcessorImpl.nop.Keyword] must beTrue
      proc.newline.isInstanceOf[EBProcessorImpl.nop.Newline] must beTrue
      proc.externalCharacter.isInstanceOf[EBProcessorImpl.nop.ExternalCharacter] must beTrue
      proc.monoGraphic.isInstanceOf[EBProcessorImpl.nop.MonoGraphic] must beTrue
      proc.colorGraphic.isInstanceOf[EBProcessorImpl.nop.ColorGraphic] must beTrue
      proc.sound.isInstanceOf[EBProcessorImpl.nop.Sound] must beTrue
      proc.text.isInstanceOf[EBProcessorImpl.html.Text] must beTrue
    }
  }
}
