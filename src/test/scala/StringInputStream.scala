package com.github.rubyu.ebquery

import java.io.ByteArrayInputStream

class StringInputStream(string: String, charset: String = "MS932")
  extends ByteArrayInputStream(string.getBytes(charset)) {
}
